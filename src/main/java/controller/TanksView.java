package controller;

import lombok.Data;
import models.FillTank;

import javax.swing.*;
import java.util.concurrent.Semaphore;

@Data
public class TanksView {
	private JButton fillButton;
	private JProgressBar progressBar1;
	private JProgressBar progressBar2;
	private JProgressBar progressBar3;
	private JProgressBar progressBar4;
	private JPanel mainPanel;
	private JProgressBar sourceTank;

	private Semaphore semaphore = new Semaphore(1);

	public TanksView() {
		fillButton.addActionListener(actionEvent -> {
			resetAllTanks();
			FillTank fillTank1 = new FillTank(progressBar1, sourceTank, semaphore);
			FillTank fillTank2 = new FillTank(progressBar2, sourceTank, semaphore);
			FillTank fillTank3 = new FillTank(progressBar3, sourceTank, semaphore);
			FillTank fillTank4 = new FillTank(progressBar4, sourceTank, semaphore);

			fillTank1.start();
			fillTank2.start();
			fillTank3.start();
			fillTank4.start();
		});
	}

	private void resetAllTanks() {
		sourceTank.setValue(400);
		progressBar1.setValue(0);
		progressBar2.setValue(0);
		progressBar3.setValue(0);
		progressBar4.setValue(0);
	}


}

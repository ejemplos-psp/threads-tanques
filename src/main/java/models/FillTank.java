package models;

import javax.swing.*;
import java.util.Arrays;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

public class FillTank extends Thread {
	private JProgressBar jProgressBar;
	private JProgressBar sourceTank;
	private Semaphore semaphore;

	public FillTank(JProgressBar jProgressBar, JProgressBar sourceTank, Semaphore semaphore) {
		this.jProgressBar = jProgressBar;
		this.sourceTank = sourceTank;
		this.semaphore = semaphore;
	}

	@Override
	public void run() {
		try {
			while (this.jProgressBar.getValue() < this.jProgressBar.getMaximum()) {
				semaphore.acquire();
				fillTank();
				semaphore.release();
				TimeUnit.MILLISECONDS.sleep(15);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			Thread.currentThread().interrupt();
		} finally {
			System.out.println(this.getName() + " read after fill: " + this.sourceTank.getValue());
		}


	}

	private void fillTank() {
		this.jProgressBar.setValue(this.jProgressBar.getValue() + 1);
		this.sourceTank.setValue(this.sourceTank.getValue() - 1);
	}
}
